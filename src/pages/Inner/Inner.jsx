import React, { useState } from "react";
import "./Inner.scss";
import Box from "../../components/Box/Box";
import Expertise from "../../components/Expertise/Expertise";
import Panel from "../../components/Panel/Panel";
import Address from "../../components/Address/Address";
import Feedback from "../../components/Feedback/Feedback";
import ButtonSmall from "./../../components/ButtonSmall/ButtonSmall";
import Portfolio from "../../components/Portfolio/Portfolio";
import Skills from "../../components/Skills/Skills";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import data from "../../utils/data";
import Education from "../../components/Education/Education";

const Inner = () => {
  const [isOpen, setIsOpen] = useState(true);

  const togglePanel = () => {
    setIsOpen(!isOpen);
  };

  const scrollUp = () => window.scrollTo({ top: 0, behavior: "smooth" });

  return (
    <div className="innerPage">
      <Panel isOpen={isOpen} toggle={togglePanel} />
      <main
        className={`main__content ${isOpen ? "narrow" : "wide"} `}
        data-testid="inner"
      >
        <Box title="About me" id="about">
          Creative and Result-driven software engineer skilled in developing
          responsive user interfaces and web applications with React ,
          JavaScript, HTML5, CSS3 and SASS. Passionate about design predictable
          and scalable engineering systems and have 8+ years of experience
          developing and delivering engineering solutions across diverse tech
          industries.Background includes designing websites and applications in
          Agile environments. Exceptional team player with an analytical
          approach to developing useful solutions.Problem
          solver.Sef-starter.Fast learner.
        </Box>
        <Box title="Education" id="education">
          <Education />
        </Box>
        <Box title="Experience" id="experience">
          <Expertise data={data.expertise} />
        </Box>
        <Box title="Skills" id="skills">
          <Skills />
        </Box>
        <Box title="Portfolio" id="portfolio">
          <Portfolio />
        </Box>
        <Box title="Contacts" id="contacts">
          <Address contactData={data.contactData} />
        </Box>
        <Box title="Feedbacks" id="feedbacks">
          <Feedback data={data.feedbacks} />
        </Box>
        <div className="scrollUp">
          <ButtonSmall clickHandler={scrollUp}>
            <FontAwesomeIcon icon={faChevronRight} />
          </ButtonSmall>
        </div>
      </main>
    </div>
  );
};

export default Inner;
