import { createServer, Model } from "miragejs";

const initialDataEducation = [
  {
    id: 1,
    date: 2021,
    title: "EPAM UpSkill",
    text: "Bootcamp EPAM Systems. Full time Frontend Developer Program",
  },
  {
    id: 2,
    date: 2019,
    title: "Moscow Institute of Physics and Technologies and Yandex",
    text: "Specialization: User Interface Development",
  },
  {
    id: 3,
    date: 2010,
    title: "Belarusian State University of Informatics and Radioelectronics",
    text: "BE, Micro- and Nanoelectronical technologies and systems",
  },
  {
    id: 4,
    date: 2007,
    title: "Minsk State Radioengineering College",
    text: "Microelectronic",
  },
];
const getDataFromStorage = (data) => JSON.parse(localStorage.getItem(data));
const setDataToStorage = (key, data) =>
  localStorage.setItem(key, JSON.stringify(data));

export default function serverSetup() {
  const server = createServer({
    models: {
      education: Model,
      skill: Model,
    },
    seeds(server) {
      server.create("skill",{item: {title: "HTML", level: "95"}});
      server.create("skill",{item: {title: "CSS", level: "85"}});
      server.create("skill",{item: {title: "Sass", level: "85"}});
      server.create("skill",{item: {title: "JavaScript", level: "75"}});
      server.create("skill",{item: {title: "TypeScript", level: "50"}});
      server.create("skill",{item: {title: "React", level: "87"}});
      server.create("skill",{item: {title: "Redux", level: "75"}});
      const prelodedData = getDataFromStorage("skills");
        if (prelodedData) {
        prelodedData.forEach((skill) => {
          server.create("skill", skill);
        });
      }
    },
    routes() {
      this.get("/api/educations", () => initialDataEducation, { timing: 3000 });

      this.get(
        "/api/skills",
        (schema) => {
          return schema.skills.all();
        },
        { timing: 3000 }
      );

      this.post("/api/skills", (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        let skillsFromLocalStore = getDataFromStorage("skills") || [];
        skillsFromLocalStore.push({ ...attrs });
        setDataToStorage("skills", skillsFromLocalStore);
        return schema.skills.create(attrs);
      });
    },
  });
}
