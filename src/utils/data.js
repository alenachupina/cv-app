const data = {
  user: {
    name: "Alena Chupina",
    title: "Frontend Developer",
    description: "Creative approach and Engineering expertise",
    url: "",
  },
  expertise: [
    {
      id: "1",
      date: "2021-2021",
      info: {
        company: "UpSkill",
        job: "Entry level Frontend developer",
        description:
          "Developed fully functional responsive cross-browser SPA with React, Redux, ES6, Sass with REST API and LocalStorage integration.",
      },
    },
    {
      id: "5",
      date: "2019-2020",
      info: {
        company: "Freelance",
        job: "UI Developer",
        description:
          "Develop a11y WCAG tested user interfaces and implement pixel perfect layouts across multiple screens according to design prototypes.",
      },
    },
    {
      id: "2",
      date: "2010-2018",
      info: {
        company: "BSESS",
        job: "Automation Design Engineer",
        description:
          "Design and implementation automation pipelines.Development SCADA integrated systems for remote monitoring and automation.",
      },
    },
    {
      id: "3",
      date: "2010",
      info: {
        company: "BMS",
        job: "Electronic Engineer, Intern",
        description: "IC design",
      },
    },
    {
      id: "4",
      date: "2007",
      info: {
        company: "Integral",
        job: "Manufacturing technician/ technologist",
        description:
          "Quality assurance discrete semiconductor devices and integrated circuits / Probe control",
      },
    },
  ],
  feedbacks: [
    {
      id: "1",
      feedback:
        " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ",
      reporter: {
        photoUrl:
          "https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg",
        name: "Anonymous",
        citeUrl: "https://www.citeexample.com",
      },
    },
    {
      id: "2",
      feedback:
        " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ",
      reporter: {
        photoUrl:
          "https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg",
        name: "John Doe",
        citeUrl: "https://www.citeexample.com",
      },
    },

    {
      id: "3",
      feedback:
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ",
      reporter: {
        photoUrl:
          "https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg",
        name: "User_123",
        citeUrl: "https://www.citeexample.com",
      },
    },
  ],
  portfolio: [
    {
      type: "ui",
      title: "Momentus",
      text: "Motivational app with personalized greeting, weather forecast and inspirational images",
      url: "https://rolling-scopes-school.github.io/chupina-JS2020Q3/momentum/",
    },
    {
      type: "code",
      title: "Github repo",
      text: "A lot of code here...",
      url: "https://github.com/chupina",
    },
    {
      type: "ui",
      title: "Virtual Piano and more...",
      text: "Open App and play the piano or accordion",
      url: "https://rolling-scopes-school.github.io/chupina-JSFE2021Q1/virtual-piano/",
    },
    {
      type: "react",
      title: "Personal CV",
      text: "Pure React custom user form",
      url: "https://gitlab.com/alenachupina/cv-app",
    },
    {
      type: "ui",
      title: "Photo filter app",
      text: "This app allows user to upload, edit and save photos ",
      url: "https://rolling-scopes-school.github.io/chupina-JSFE2021Q1/photo-filter/",
    },
    {
      type: "code",
      title: "Gitlab Repo",
      text: "A few iteresting projects here",
      url: "https://gitlab.com/alenachupina",
    },
    {
      type: "react",
      title: "Personal CV",
      text: "Personal Resume application to showcase professional information",
      url: "https://gitlab.com/alenachupina/cv-app",
    },
    {
      type: "code",
      title: "Community Website",
      text: "Tech community webste",
      url: "https://gitlab.com/alenachupina/javascript-fronted-applied-personal-website",
    },
    {
      type: "ui",
      title: "Animation",
      text: "CSS Animated multi-layer background",
      url: "https://gitlab.com/alenachupina/css-animations",
    },
    {
      type: "code",
      title: "Decorators",
      text: "Typescript solution",
      url: "https://gitlab.com/alenachupina/decorators",
    },
    {
      type: "react",
      title: "Community Website React",
      text: "Tech community webste sections created with react",
      url: "https://gitlab.com/alenachupina/react-website",
    },
    {
      type: "code",
      title: "Community Website",
      text: "Tech community webste",
      url: "https://gitlab.com/alenachupina/javascript-fronted-applied-personal-website",
    },
  ],

  contactData: [
    {
      id: "1",
      address: "tel:+16312294822",
      title: "(631) 229-4822",
      icon: "faPhoneAlt",
    },
    {
      id: "2",
      address: "mailto:chupina.elen@gmail.com",
      title: "chupina.elen@gmail.com",
      icon: "faEnvelope",
    },
    {
      id: "3",
      address: "https://www.linkedin.com/in/alena-chupina-652851210/",
      title: "LinkedIn",
      subtitle: "",
      icon: "faLinkedinIn",
    },
    {
      id: "4",
      address: "https://www.facebook.com/elena.chupina.714/",
      title: "Facebook",
      subtitle: "",
      icon: "faFacebookF",
    },
    {
      id: "5",
      address: "https://join.skype.com/invite/vDqLZ1E83tfh",
      title: "Skype",
      subtitle: "",
      icon: "faSkype",
    },
  ],
};

export default data;
